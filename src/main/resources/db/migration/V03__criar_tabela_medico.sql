CREATE TABLE medico(
	
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    token VARCHAR(200),
    nome VARCHAR(30) NOT NULL,
    especialidade VARCHAR(30) NOT NULL,
    agendamentos_hoje BIGINT(20),
    FOREIGN KEY (agendamentos_hoje) REFERENCES agendamento(codigo)
    
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO medico(nome,especialidade) VALUES('Dr. Robert Trupman','Neurologia');
INSERT INTO medico(nome,especialidade) VALUES('Dr(a). Alexia Ramos','Oftalmologia');
INSERT INTO medico(nome,especialidade) VALUES('Dr(a). Miranda Soares','Cardiologia');