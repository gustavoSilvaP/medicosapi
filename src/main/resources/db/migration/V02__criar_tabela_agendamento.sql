CREATE TABLE agendamento(
	
	codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    id_paciente BIGINT(20) NOT NULL,
    data_hora_atendimento TIMESTAMP NOT NULL,
    medico BIGINT(20) NOT NULL
    
)ENGINE=InnoDB DEFAULT CHARSET=utf8;