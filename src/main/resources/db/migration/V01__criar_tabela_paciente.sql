CREATE TABLE paciente(
	
	codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(50) NOT NULL,
    cpf VARCHAR(15) NOT NULL,
    idade BIGINT(3) NOT NULL,
    telefone VARCHAR(15) NOT NULL
	
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO paciente (nome,cpf,idade,telefone) values ('Rogerio Flores','135.193.095-35',36,'(11) 98226-5748');
INSERT INTO paciente (nome,cpf,idade,telefone) values ('Magda Faria','735.498.025-95',48,'(12) 99126-5048');
INSERT INTO paciente (nome,cpf,idade,telefone) values ('Rui Barbosa','634.111.512-32',59,'(11) 98126-4040');
INSERT INTO paciente (nome,cpf,idade,telefone) values ('Rosana Furtado','665.874.012-45',23,'(15) 3048-2355');