package com.conexa.medicosapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan(basePackages = {"com.conexa.medicosapi.model"})
@SpringBootApplication
public class MedicosapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedicosapiApplication.class, args);
	}

}
