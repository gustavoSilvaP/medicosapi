package com.conexa.medicosapi.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.conexa.medicosapi.model.Paciente;
import com.conexa.medicosapi.repository.PacienteRepository;

@Service
public class PacienteService {

	@Autowired
	private PacienteRepository pacienteRepository;
	
	
	public Paciente atualizar(Long codigo, Paciente paciente) {
		
		Paciente pacienteSalvo = buscarPacientePeloCodigo(codigo);
		BeanUtils.copyProperties(paciente, pacienteSalvo, "codigo");
		return this.pacienteRepository.save(pacienteSalvo);
	}
	
	public Paciente buscarPacientePeloCodigo(Long codigo) {
		Paciente pacienteSalvo = this.pacienteRepository.findById(codigo).orElseThrow(
				() -> new EmptyResultDataAccessException(1));
		return pacienteSalvo;
	}
}