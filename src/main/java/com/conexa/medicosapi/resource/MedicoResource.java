package com.conexa.medicosapi.resource;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.conexa.medicosapi.model.Medico;
import com.conexa.medicosapi.repository.MedicoRepository;

@RestController
@RequestMapping("/medicos")
public class MedicoResource {

	@Autowired
	private MedicoRepository medicoRepository;
	
	
	@GetMapping
	public List<Medico> listar(){
		
		return medicoRepository.findAll();
	}
	
	@GetMapping("/{codigo}")
	public ResponseEntity<Medico> buscarPeloCodigo(@PathVariable Long codigo, HttpServletResponse response){

		Optional<Medico> codigoMedico = this.medicoRepository.findById(codigo);
		
		return codigoMedico.isPresent() ? ResponseEntity.ok(codigoMedico.get())	
				: ResponseEntity.notFound().build();
	}
	
}