package com.conexa.medicosapi.resource;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.conexa.medicosapi.event.RecursoCriadoEvent;
import com.conexa.medicosapi.model.Agendamento;
import com.conexa.medicosapi.model.Medico;
import com.conexa.medicosapi.model.Paciente;
import com.conexa.medicosapi.repository.AgendamentoRepository;
import com.conexa.medicosapi.repository.MedicoRepository;
import com.conexa.medicosapi.repository.PacienteRepository;

@RestController
@RequestMapping("/agendamentos")
public class AgendamentoResource {

	@Autowired
	private AgendamentoRepository agendamentoRepository;
	
	@Autowired
	private MedicoRepository medicoRepository;
	
	@Autowired
	private PacienteRepository pacienteRepository;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	
	@GetMapping
	public List<Agendamento> listar(){
		
		return agendamentoRepository.findAll();
	}
	
	@GetMapping("/{codigo}")
	public ResponseEntity<Agendamento> buscarPeloCodigo(@PathVariable Long codigo, HttpServletResponse response){

		Optional<Agendamento> codigoAgendamento = this.agendamentoRepository.findById(codigo);
		
		return codigoAgendamento.isPresent() ? ResponseEntity.ok(codigoAgendamento.get()) 
						: ResponseEntity.notFound().build();
	}
	
	@PostMapping
	public ResponseEntity<Agendamento> criar(@Valid @RequestBody Agendamento agendamento, HttpServletResponse response) {
		
		Optional<Medico> agendamentoCodigoMedico = medicoRepository.findById(agendamento.getMedico());
		Optional<Paciente> agendamentoCodigoPaciente = pacienteRepository.findById(agendamento.getIdPaciente());
		
		if(agendamentoCodigoMedico.isPresent() && agendamentoCodigoPaciente.isPresent()) {
			
			Agendamento agendamentoSalvo = agendamentoRepository.save(agendamento);
			publisher.publishEvent(new RecursoCriadoEvent(this, response, agendamentoSalvo.getCodigo()));
		
			return ResponseEntity.status(HttpStatus.CREATED).body(agendamentoSalvo);
		}
		
		return ResponseEntity.notFound().build();
	}
}