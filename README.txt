Eu estou aqui tentando fazer o teste, e existem algumas coisas nele que por nunca ter visto mesmo, eu não consegui fazer. 

A parte do crud foi bem simples, 
atrelar os agendamentos ao médico também, mas tive um pouco de resistência quando fui tentar listar o médico em si e suas consultas,
o JWT foi uma coisa que descobri por conta deste teste, mas aprendi um pouco dele, 
aprendi como gerar o token e usá-lo para que um usuário possa usar as rotas da API.

Estou usando access_token para acessos.

Banco utilizado: MySql
Foi utilizado també, Flyway para migração.

=====================================================================================================================================


Usei o postman para efetuar todas as requisições, deixarei a baixo:

Listar Pacientes: "GET" localhost:8080/pacientes
Buscar Paciente por código: "GET" localhost:8080/pacientes/2
Novo Paciente: "POST" localhost:8080/pacientes
Remover Paciente: "DELETE" localhost:8080/pacientes/6
Atualizar Paciente: "PUT" localhost:8080/pacientes/2

Listar Medicos: "GET" localhost:8080/medicos
Buscar Medicos por código: "GET" localhost:8080/medicos/1

Listar Agendamentos: "GET" localhost:8080/agendamentos
Novo Agendamento: "POST" localhost:8080/agendamentos

Gerar Token de acesso aos metodos: "POST" localhost:8080/oauth/token
urlencoded -> KEY       |   VALUE
			  client        conexa 
			  username      medico@gmail.com
			  password      admin
			  grant_type    password
			  
Acesso do client a API -> Usuário: conexao | Senha: conex@